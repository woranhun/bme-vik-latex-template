#!/bin/bash
/usr/bin/find $(dirname "$0") -name *.drawio -exec rm -f {}.pdf \; -exec /usr/bin/drawio --crop -x -o {}.pdf {} \;
