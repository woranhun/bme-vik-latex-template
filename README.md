# BME-VIK LaTeX template

This repository was built with the help of the [Official BME VIK Thesis Template](https://diplomaterv.vik.bme.hu/Download/DiplomatervSablon_LaTeX_v1.zip).

Use it at your own risk.
